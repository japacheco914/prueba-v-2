#language: es
# new feature
# Tags: optional

Característica: Iniciar sesion y cerrar sesion
  yo como usuario deseo poder ingresar a la plataforma
  y luego de haber interactuado con la plataforma
  poder cerrar sesion

  Escenario: Iniciar sesion y cerrar sesion
    Dado que el usuario desea iniciar sesion
    Cuando ya termina de interactuar con la pagina desea cerrar sesion
    Entonces el usuario podra iniciar sesion y cerrar sesion cuando lo desee
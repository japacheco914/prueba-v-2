package co.com.sofka.runners.loginandsignout;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/loginandsignout"},
        glue = {"co.com.sofka.stepsdefinitions.loginandsignout"})
public class LoginAndSignOutRunner {
}

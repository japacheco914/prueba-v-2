package co.com.sofka.runners.checkout;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/checkout/checkout.feature"},
        glue ="co.com.sofka.stepsdefinitions.checkout"
)
public class CheckoutRunner {
}

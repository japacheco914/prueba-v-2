package co.com.sofka.stepsdefinitions.loadmorebutton;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.hamcrest.Matchers;

import static co.com.sofka.question.loadmorebutton.LoadMoreButtonQuestion.loadMoreButtonQuestion;
import static co.com.sofka.task.login.LlenarCamposLogin.llenarCamposLogin;
import static co.com.sofka.task.login.NavegateToLogin.navegateToLogin;
import static co.com.sofka.task.loadmorebutton.NavegateToLoadMore.navegateToLoadMore;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LoadMoreButtonStepDefinition extends Setup {

    Faker faker = new Faker();
    private static final String ACTOR_NAME = "JohanM";

    @Dado("El usuario estaba logueado en la pagina web")
    public void elUsuarioEstabaLogueadoEnLaPaginaWeb() {

        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().wasAbleTo(

                navegateToLogin(),

                llenarCamposLogin()
                        .emailAddressLogin(faker.internet().emailAddress())
                        .passwordLogin(faker.internet().password())

        );
    }

    @Cuando("El usuario accede al contenido del boton cargar mas para elegir")
    public void elUsuarioAccedeAlContenidoDelBotonCargarMasParaElegir() {

        theActorInTheSpotlight().wasAbleTo(

                navegateToLoadMore());

    }

    @Entonces("El usuario vera el ultimo destino")
    public void elUsuarioVeraElUltimoDestino() {

        theActorInTheSpotlight().should(
                seeThat(loadMoreButtonQuestion(), Matchers.equalTo(true)));
        
    }
}

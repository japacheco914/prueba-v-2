package co.com.sofka.stepsdefinitions.loginandsignout;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.question.loginandsignout.LoginAndSignOutError.loginAndSignOutError;
import static co.com.sofka.task.login.LlenarCamposLogin.llenarCamposLogin;
import static co.com.sofka.task.login.NavegateToLogin.navegateToLogin;
import static co.com.sofka.task.loginandsignout.LoginAndSignOut.loginAndSignOut;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;


public class LoginAndSignOutStepDefinition extends Setup {


    Faker faker = new Faker();
    private static final String ACTOR = "Person";

    @Dado("que el usuario desea iniciar sesion")
    public void queElUsuarioDeseaIniciarSesion() {

        actorSetupTheBrowser(ACTOR);
        theActorInTheSpotlight().wasAbleTo(
               // openLandingPage(),
                navegateToLogin()

        );
        theActorInTheSpotlight().wasAbleTo(
                llenarCamposLogin()
                        .emailAddressLogin(faker.internet().emailAddress())
                        .passwordLogin(faker.internet().password())
        );
    }

    @Cuando("ya termina de interactuar con la pagina desea cerrar sesion")
    public void yaTerminaDeInteractuarConLaPaginaDeseaCerrarSesion() {

        theActorInTheSpotlight().attemptsTo(loginAndSignOut());

    }

    @Entonces("el usuario podra iniciar sesion y cerrar sesion cuando lo desee")
    public void elUsuarioPodraIniciarSesionYCerrarSesionCuandoLoDesee() {

      theActorInTheSpotlight().should(
              seeThat(loginAndSignOutError().is(),equalTo(true))
      );
    }



}

package co.com.sofka.task.checkout;

import co.com.sofka.model.checkout.CheckoutModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.sofka.userinterface.checkout.Checkout.*;
import static co.com.sofka.util.Dictionary.COUNTRY_CODE;
import static co.com.sofka.util.Dictionary.SPANISH_CODE_LANGUAGE;
import static co.com.sofka.util.Helper.generarCheckout;

public class LlenarCheckout implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        CheckoutModel checkoutModel = generarCheckout(SPANISH_CODE_LANGUAGE, COUNTRY_CODE);

        actor.attemptsTo(
                Enter.theValue(checkoutModel.getName()).into(NAME),
                Enter.theValue(checkoutModel.getEmail()).into(EMAIL),
                Enter.theValue(checkoutModel.getSocialNumber()).into(SSN),
                Enter.theValue(checkoutModel.getPhone()).into(PHONE_NUMBER),
                Click.on(PROMO_CODE),
                Click.on(PAY_BTN)
        );
    }
    public static LlenarCheckout llenarCheckout(){
        return new LlenarCheckout();
    }
}

package co.com.sofka.task.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.login.Login.*;

public class LlenarCamposLogin implements Task {

    private String emailAddressLogin;
    private String passwordLogin;

    public LlenarCamposLogin emailAddressLogin(String emailAddressLogin) {
        this.emailAddressLogin = emailAddressLogin;
        return this;
    }

    public LlenarCamposLogin passwordLogin(String passwordLogin) {
        this.passwordLogin = passwordLogin;
        return this;
    }



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
        Scroll.to(USER_NAME),
                Enter.theValue(emailAddressLogin).into(USER_NAME),

                Scroll.to(PASSWORDLOGIN),
                Enter.theValue(passwordLogin).into(PASSWORDLOGIN),

                Click.on(BTN_LOGIN)
);
    }
    public static LlenarCamposLogin llenarCamposLogin(){
        return new LlenarCamposLogin();
    }
}

package co.com.sofka.question.checkout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterface.checkout.Checkout.TERMS_AND_CONDITIONS_ALERT;
import static co.com.sofka.util.Dictionary.TERMS_AND_CONDITIONS_ALERT_MESSAGE;

public class Checkout implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        return (TERMS_AND_CONDITIONS_ALERT.resolveFor(actor).containsOnlyText(TERMS_AND_CONDITIONS_ALERT_MESSAGE));
    }
    public static Checkout termsAndConditionsValidation(){
        return new Checkout();
    }
}

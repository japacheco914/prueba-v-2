package co.com.sofka.util;

public abstract class Dictionary {
    public static final String WEB_URL = "https://demo.testim.io";
    public static final String USER_NAME = "User";
    public static final String SPANISH_CODE_LANGUAGE = "es";
    public static final String COUNTRY_CODE = "US";
    public static final String TERMS_AND_CONDITIONS_ALERT_MESSAGE = "Terms and Conditions";
    public static final String LOGO_MESSAGE_LOG_OUT = "LOG IN";

    private Dictionary() {
    }
}

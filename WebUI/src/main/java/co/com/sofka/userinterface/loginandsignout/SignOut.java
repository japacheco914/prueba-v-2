package co.com.sofka.userinterface.loginandsignout;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SignOut extends PageObject {

    public static final Target HELLO = Target
            .the("Hello")
            .located(By.xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/div/button"));

    public static final Target SIGN_OUT = Target
            .the("Log out")
            .located(By.xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/div/ul/li/a"));


    public static final Target BUTTON_LOGIN = Target
            .the("LOG IN")
            .located(By.xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/li/button"));
}

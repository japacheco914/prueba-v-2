package co.com.sofka.userinterface.filtro;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class Filtro extends PageObject {

    public static final Target LIST = Target
            .the("Lista colores")
            .located(By.xpath("//*[@id='app']/div/section[2]/div[3]/div/div/div[2]/div/input"));

    public static final Target PURPLE_COLOR = Target
            .the("color purpura")
            .located(By.xpath("//*[@id='app']/div/section[2]/div[3]/div/div/div[2]/ul/li[6]"));

    public static final Target SHAHEYING = Target
            .the("Nombre destino Shaheying")
            .located(By.xpath("//*[@id='app']/div/section[2]/div[4]/div/div/div[1]/div[2]/div/h5"));

}

package co.com.sofka.userinterface.precios;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PreciosPage extends PageObject {

    public static final Target CAMPO_PRICE =
            Target
                    .the("Precios")
                    .located(By.xpath("(//input[@class='theme__inputElement___27dyY theme__filled___1UI7Z'])[2]"));

    public static final Target CAMPO_VALIDATION =
            Target.the("Viaje")
                    .located(By.xpath("//div[4]/div/div/div[2]/div/div"));

    public static final Target TITULO_ROLL =
            Target.the("Titulo")
                    .located(By.xpath("//section[2]/div/h2"));

    public static final Target TEXT_CONFIRM =
            Target.the("Viaje")
                    .located(By.xpath("//div[1]/div[2]/div/h5"));


}

# new feature
# Tags: optional
#language: es

Característica: Paleta de colores
como diseñador grafico
quiero poder conocer el codigo hexadecimal
para poder generar la paleta de colores de mi cartelera


  Escenario: Obtener el codigo de un solo color
    Dado que estoy en el servicio de paleta de colores
    Cuando solicite el codigo de color fucsia
    Entonces se obtendra un status 200 y el codigo correspondiente

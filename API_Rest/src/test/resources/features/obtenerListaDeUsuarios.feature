# language: es
Característica: obtener lista de usuarios
  como administrador
  deseo obtener la lista de usuarios
  para poder validar roles y permisos

  Escenario: obtener todos los usuarios
    Dado que como administrador estoy el modulo de usuarios
    Cuando solicite el listado de todos los usuarios
    Entonces podre ver la lista completa de usuarios

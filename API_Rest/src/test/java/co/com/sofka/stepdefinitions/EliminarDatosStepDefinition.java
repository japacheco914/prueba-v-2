package co.com.sofka.stepdefinitions;

import co.com.sofka.question.TheStatusCode;
import co.com.sofka.task.EliminarDatos;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class EliminarDatosStepDefinition {
    private String ACTOR = "Pepito";

    @Dado("el usuario esta en la plataforma y desea eliminar datos")
    public void elUsuarioEstaEnLaPlataformaYDeseaEliminarDatos() {
        setTheStage(new OnlineCast());
        theActorCalled(ACTOR);
    }

    @Cuando("el usuario realiza la peticion de eliminar datos")
    public void elUsuarioRealizaLaPeticionDeEliminarDatos() {
        theActorInTheSpotlight().attemptsTo(
                EliminarDatos.eliminarDatos()
        );
    }

    @Entonces("el usuario obtendra un codigo de respuesta {int}")
    public void elUsuarioObtendraUnCodigoDeRespuesta(Integer code) {
        theActorInTheSpotlight().should(
                seeThat(TheStatusCode.is(code))
        );
    }
}

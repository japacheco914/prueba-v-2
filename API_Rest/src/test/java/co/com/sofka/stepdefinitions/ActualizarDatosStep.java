package co.com.sofka.stepdefinitions;

import co.com.sofka.common.Setup;
import co.com.sofka.model.update.ActualizarDatosRespuesta;
import co.com.sofka.model.update.CatosModelActualizarDatos;
import co.com.sofka.question.ActualizarDatosQuestion;
import co.com.sofka.util.LeerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import static co.com.sofka.task.DoActualizarDatos.doPatch;
import static co.com.sofka.util.Dictionary.RESOURCE;
import static co.com.sofka.util.Dictionary.URL_BASE;
import static co.com.sofka.util.Persona.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;

public class ActualizarDatosStep extends Setup {


    private final Actor actor = Actor.named("AlvaroD");
    private String bodyRequest;
    private CatosModelActualizarDatos datosRandom;

    private LeerArchivoJson archivoJson;
    private static final Logger LOGGER = Logger.getLogger(ActualizarDatosStep.class);

    @Dado("el usuario esta en la plataforma y desea actualizar datos")
    public void elUsuarioEstaEnLaPlataformaYDeseaActualizarDatos() {

        try{
            generalSetUp();
            actor.can(CallAnApi.at(URL_BASE));

            datosRandom = generarPersonasRandom();
            archivoJson = new LeerArchivoJson(datosRandom.getJob(), datosRandom.getFullName() );
            String SaveNameJob = archivoJson.convertPatch();
            bodyRequest = SaveNameJob;
            LOGGER.info(bodyRequest);

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }
    }

    @Cuando("el usuario logra actualizar los datos")
    public void elUsuarioLograActualizarLosDatos() {

        try{

            actor.attemptsTo(
                    doPatch()
                            .usingTheResource(RESOURCE)
                            .andBodyRequest(bodyRequest)
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }

    @Entonces("el usuario obtendra un codigo de respuesta exitosa y podra ver sus datos actualizados")
    public void elUsuarioObtendraUnCodigoDeRespuestaExitosaYPodraVerSusDatosActualizados() {

        try{

            LastResponse.received().answeredBy(actor).prettyPrint();

            ActualizarDatosRespuesta Customer = new ActualizarDatosQuestion().answeredBy(actor);
            actor.should(
                    seeThatResponse(
                            "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                    seeThat(
                            "El nombre del usuario es:", x -> Customer.getName(), equalTo(datosRandom.getFullName())),
                    seeThat(
                            "El trabajo del usuario es:", x -> Customer.getJob(), equalTo(datosRandom.getJob()))
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }
}

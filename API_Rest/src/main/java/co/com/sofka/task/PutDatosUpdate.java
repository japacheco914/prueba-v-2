package co.com.sofka.task;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;

public class PutDatosUpdate implements Task {

    private String resource;
    private String bodyRequest;

    public PutDatosUpdate usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public PutDatosUpdate andBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to(resource)
                        .with(
                                requestSpecification ->requestSpecification.relaxedHTTPSValidation().log().all()
                                        .contentType(ContentType.JSON)
                                        .body(bodyRequest)
                        )
        );

    }
    public static PutDatosUpdate doPut(){
        return new PutDatosUpdate();
    }
}

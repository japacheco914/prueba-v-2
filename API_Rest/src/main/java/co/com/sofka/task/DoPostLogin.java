package co.com.sofka.task;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class DoPostLogin implements Task {
    private String resource;
    private String bodyRequest;

    public DoPostLogin usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoPostLogin andBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(resource).with(requestSpecification -> requestSpecification.relaxedHTTPSValidation()
                        .contentType(ContentType.JSON)
                        .body(bodyRequest))
        );
    }

    public static DoPostLogin doPostLogin() {
        return new DoPostLogin();
    }
}

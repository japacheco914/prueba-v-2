package co.com.sofka.model.create;

public class CreateRespuesta {
    private String name;
    private String job;
    private String id;

    public String getName(){return name;}
    public String getJob(){return job;}
    public String getId(){return id;}
}

package co.com.sofka.model.update;

public class CatosModelActualizarDatos {

    private String fullName;
    private String job;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}

package co.com.sofka.model.listusers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"page",
"per_page",
"total",
"total_pages",
"data",
"support"
})
public class UsersData {

@JsonProperty("page")
private int page;
@JsonProperty("per_page")
private int perPage;
@JsonProperty("total")
private int total;
@JsonProperty("total_pages")
private int totalPages;
@JsonProperty("data")
private List<Datum> data = null;
@JsonProperty("support")
private SupportListUsers supportListUsers;
@JsonIgnore
private final Map<String, Object> additionalProperties = new HashMap<>();

@JsonProperty("page")
public int getPage() {
return page;
}

@JsonProperty("page")
public void setPage(int page) {
this.page = page;
}

@JsonProperty("per_page")
public int getPerPage() {
return perPage;
}

@JsonProperty("per_page")
public void setPerPage(int perPage) {
this.perPage = perPage;
}

@JsonProperty("total")
public int getTotal() {
return total;
}

@JsonProperty("total")
public void setTotal(int total) {
this.total = total;
}

@JsonProperty("total_pages")
public int getTotalPages() {
return totalPages;
}

@JsonProperty("total_pages")
public void setTotalPages(int totalPages) {
this.totalPages = totalPages;
}

@JsonProperty("data")
public List<Datum> getData() {
return data;
}

@JsonProperty("data")
public void setData(List<Datum> data) {
this.data = data;
}

@JsonProperty("support")
public SupportListUsers getSupportListUsers() {
return supportListUsers;
}

@JsonProperty("support")
public void setSupportListUsers(SupportListUsers supportListUsers) {
this.supportListUsers = supportListUsers;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
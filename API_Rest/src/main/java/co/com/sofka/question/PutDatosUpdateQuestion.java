package co.com.sofka.question;

import co.com.sofka.model.update.ActualizarDatosRespuesta;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class PutDatosUpdateQuestion implements Question<ActualizarDatosRespuesta> {

    @Override
    public ActualizarDatosRespuesta answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(ActualizarDatosRespuesta.class);
    }
}

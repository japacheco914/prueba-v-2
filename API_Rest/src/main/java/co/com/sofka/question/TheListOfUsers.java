package co.com.sofka.question;

import co.com.sofka.model.listusers.UsersData;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheListOfUsers implements Question<UsersData> {
    @Override
    public UsersData answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(UsersData.class);
    }

    public static TheListOfUsers theListOfUsers(){
        return  new TheListOfUsers();
    }
}

package co.com.sofka.question;

import co.com.sofka.model.create.CreateRespuesta;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CreateQuestion implements Question<CreateRespuesta> {

    @Override
    public CreateRespuesta answeredBy(Actor actor){
        return SerenityRest.lastResponse().as(CreateRespuesta.class);
    }
}

package co.com.sofka.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheStatusCode implements Question<Boolean> {
    private int code;

    public TheStatusCode(int code) {
        this.code = code;
    }

    public static TheStatusCode is(int code){
        return new TheStatusCode(code);
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        return SerenityRest.lastResponse().statusCode() == code;
    }
}

package co.com.sofka.util;

public enum PalabrasClaves {
    COMPLETENAME("[name]"),
    JOB("[job]");

    private final String value;

    PalabrasClaves(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

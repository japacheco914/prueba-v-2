package co.com.sofka.util;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import static co.com.sofka.util.Dictionary.EMPTY_STRING;

public class FileReader {
    private static final Logger LOGGER= Logger.getLogger(FileReader.class);

    private static final String USER_DIR = System.getProperty("user.dir");
    private static final ArrayList<String> FILE_FOLDER_ABS_PATH_LIST = new ArrayList<>(Arrays
    .asList(USER_DIR, "src", "test", "resources", "archivosjson"));
    private String fileAbsolutePath;

    public FileReader(String fileName) {
        try {
            FILE_FOLDER_ABS_PATH_LIST.add(fileName);
        } catch (Exception e) {
            LOGGER.error("Ocurrio un error al adicionar el nombre de archivo a la ruta: ",e);
            LOGGER.error(e.getMessage(),e);
        }
    }

    private void filePathJoiner() {

        String fileSeparator = FileSystems.getDefault().getSeparator();
        this.fileAbsolutePath = String.join(fileSeparator, FILE_FOLDER_ABS_PATH_LIST);
        LOGGER.info("La ruta absoluta del archivo es: "+this.fileAbsolutePath);
    }

    public String readContent() {

        String content = EMPTY_STRING;
        filePathJoiner();
        try {
            content = new String(
                    Files.readAllBytes(
                            Paths.get(this.fileAbsolutePath)));
        } catch (IOException e) {
            LOGGER.error("Ocurrió un error al leer el archivo \n");
            LOGGER.error(e.getMessage(),e);
            LOGGER.error(e.getStackTrace(),e);
        }
        LOGGER.info("Con contenido del archivo es:\n"+content);
        return content;
    }
}

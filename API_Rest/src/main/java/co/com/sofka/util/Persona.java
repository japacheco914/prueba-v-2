package co.com.sofka.util;

import co.com.sofka.model.update.CatosModelActualizarDatos;
import com.github.javafaker.Faker;

public class Persona {


    public static CatosModelActualizarDatos generarPersonasRandom(){
        //Datos Random
        Faker datosRandom = new Faker();
        String firstName = datosRandom.name().firstName();
        String lastName = datosRandom.name().lastName();
        String fullName = firstName + " " + lastName;
        String employ = datosRandom.job().title();

        CatosModelActualizarDatos random = new CatosModelActualizarDatos();
        random.setFullName(fullName);
        random.setJob(employ);

        return random;
    }

    private Persona() {
    }
}

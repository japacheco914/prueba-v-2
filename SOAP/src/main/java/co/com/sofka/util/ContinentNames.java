package co.com.sofka.util;

public enum ContinentNames {
    AFRICA("Africa"),
    ANTARCTICA("Antarctica"),
    ASIA("Asia"),
    EUROPE("Europe"),
    OCEANIA("Ocenania"),
    THE_AMERICAS("The Americas");

    private final String value;

    ContinentNames(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
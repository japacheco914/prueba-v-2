package co.com.sofka.util;

public enum CountryCurrencyKeys {
    COUNTRY_ISO_CODE("[countryISOCode]");

    private final String value;

    CountryCurrencyKeys(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}

package co.com.sofka.stepdefinitions.listofcontinents;

import co.com.sofka.common.ServiceSetup;
import co.com.sofka.util.ContinentNames;
import co.com.sofka.util.FileReader;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static co.com.sofka.question.TheResponseCode.theResponseCodeIs;
import static co.com.sofka.question.listofcontinents.TheListOfContinents.theListOfContinents;
import static co.com.sofka.task.DoPost.doPost;
import static co.com.sofka.util.Dictionary.EMPTY_STRING;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.apache.http.entity.ContentType.TEXT_XML;

public class ListOfContinentsStepDefinitions extends ServiceSetup {

    private static final Logger LOGGER = Logger.getLogger(ListOfContinentsStepDefinitions.class);

    private final Actor theActor = Actor.named("Jose, el analista geopolitico");

    @Dado("que el analista geopolitico ha ingresado al recurso de listar nombres")
    public void queElAnalistaGeopoliticoHaIngresadoAlRecursoDeListarNombres() {

        try {
            generalSetUp();
            FileReader reader = new FileReader(LIST_OF_CONTINENTS_XML_FILE);
            bodyRequest = reader.readContent();
            LOGGER.info("El cuerpo del request es:\n " + bodyRequest);
            headers.put(CONTENT_TYPE, TEXT_XML.withCharset(StandardCharsets.UTF_8).toString());
            headers.put(SOAP_ACTION, EMPTY_STRING);

            theActor.can(CallAnApi.at(BASE_URI));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("solicite el listado de nombres de los continentes")
    public void soliciteElListadoDeNombresDeLosContinentes() {
        try {
            theActor.attemptsTo(
                    doPost()
                            .usingThe(RESOURCE)
                            .with(headers)
                            .and(bodyRequest)
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("obtendra el listado de los codigos ISO y nombres de los continentes")
    public void obtendraElListadoDeLosCodigosISOYNombresDeLosContinentes() {

        LOGGER.info("Serenity content type\n" + SerenityRest.lastResponse().getContentType());
        LOGGER.info("Response toString()\n"+theListOfContinents().answeredBy(theActor));

        try {

            List<String> continentList=new ArrayList<>();

            continentList.add(ContinentNames.AFRICA.getValue());
            continentList.add(ContinentNames.ANTARCTICA.getValue());
            continentList.add(ContinentNames.EUROPE.getValue());
            continentList.add(ContinentNames.OCEANIA.getValue());
            continentList.add(ContinentNames.THE_AMERICAS.getValue());

            theActor.should(
                    seeThat(theResponseCodeIs(), Matchers.equalTo(HttpStatus.SC_OK)),
                    seeThat("La lista de continentes es: ", theListOfContinents(),
                            Matchers.stringContainsInOrder(continentList))
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(),e);
        }

    }

}

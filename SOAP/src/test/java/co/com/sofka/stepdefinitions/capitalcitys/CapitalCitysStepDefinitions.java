package co.com.sofka.stepdefinitions.capitalcitys;

import co.com.sofka.common.ServiceSetup;
import co.com.sofka.util.FileReader;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import java.nio.charset.StandardCharsets;
import static co.com.sofka.question.TheResponseCode.theResponseCodeIs;
import static co.com.sofka.question.capitalcitys.CapitalCitysQuest.capitalCitysQuest;
import static co.com.sofka.task.DoPost.doPost;
import static co.com.sofka.util.CapitalCityKey.ISO_CODE;
import static co.com.sofka.util.Dictionary.EMPTY_STRING;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.apache.http.entity.ContentType.TEXT_XML;
import static org.hamcrest.CoreMatchers.notNullValue;

public class CapitalCitysStepDefinitions extends ServiceSetup {

 private static final Logger LOGGER = Logger.getLogger(CapitalCitysStepDefinitions.class);
 Faker faker = new Faker();
 private final Actor Carto = Actor.named("Cartografo") ;

    @Dado("El Cartografo estaba ubicado en la pagina")
    public void elCartografoEstabaUbicadoEnLaPagina() {

            try {
                    generalSetUp();
                    FileReader reader = new FileReader(CAPITAL_CITYS_XML_FILE);
                    bodyRequest = reader.readContent().replace(ISO_CODE.getValue(),
                            faker.country().countryCode2().toUpperCase());
                    headers.put(CONTENT_TYPE, TEXT_XML.withCharset(StandardCharsets.UTF_8).toString());
                    headers.put(SOAP_ACTION, EMPTY_STRING);
                    Carto.can(CallAnApi.at(BASE_URI));

            } catch (Exception e) {
                LOGGER.error("Ha ocurrido un error 3");
                Assertions.fail("Ha ocurrido un error 3");
            }
    }

    @Cuando("ingresa el Iso Code de un pais")
    public void ingresaElIsoCodeDeUnPais() {

            try {
                    Carto.attemptsTo(
                            doPost()
                                    .usingThe(RESOURCE)
                                    .with(headers)
                                    .and(bodyRequest));

            } catch (Exception e) {
                LOGGER.error("Ha ocurrido un error 3");
                Assertions.fail("Ha ocurrido un error 3");
            }
    }

    @Entonces("recibe en pantalla la capital de dicho pais")
    public void recibeEnPantallaLaCapitalDeDichoPais() {

        try {
                Carto.should(
                        seeThat(theResponseCodeIs(), Matchers.equalTo(HttpStatus.SC_OK)),
                        seeThat(capitalCitysQuest(), notNullValue()),
                        seeThat("La capital es: ",capitalCitysQuest(),
                                Matchers.containsString(LastResponse.received().answeredBy(Carto).prettyPrint()))

                );

        } catch (Exception e) {
            LOGGER.error("Ha ocurrido un error 3");
           Assertions.fail("Ha ocurrido un error 3");
        }
    }
}

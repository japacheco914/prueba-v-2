package co.com.sofka.runners;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberSerenityRunner;
import org.junit.runner.RunWith;

@RunWith(CucumberSerenityRunner.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/countryinformacion.feature"},
        glue = { "co.com.sofka.stepdefinitions"}
)

public class CountryInfoTest {
}


